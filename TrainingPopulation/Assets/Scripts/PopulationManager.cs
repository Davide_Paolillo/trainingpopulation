﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopulationManager : MonoBehaviour
{
    [SerializeField] private GameObject person;
    [SerializeField] private int populationSize = 10;

    [SerializeField] private int mutationRatio = 85;

    public static float elapsed = .0f;
    private float sessionTimer = 10.0f;

    private List<GameObject> population;
    private int generation = 1;

    GUIStyle guiStyle = new GUIStyle();
    private void OnGUI()
    {
        guiStyle.fontSize = 50;
        guiStyle.normal.textColor = Color.white;
        GUI.Label(new Rect(10, 10, 100, 20), "Generation: " + generation, guiStyle);
        GUI.Label(new Rect(10, 65, 100, 20), "Trial Time: " + (int) elapsed, guiStyle);
    }

    private void Awake()
    {
        population = new List<GameObject>();
        GeneratePopulation();
    }

    private void Update()
    {
        elapsed += Time.deltaTime;
        if (elapsed > sessionTimer)
            SpawnNewPopulation();
    }

    private void GeneratePopulation()
    {
        for (int i = 0; i < populationSize; i++)
        {
            Vector2 position = new Vector2(UnityEngine.Random.Range(-9.0f, 9.0f), UnityEngine.Random.Range(-4.0f, 4.0f));
            GameObject human = Instantiate(person, position, Quaternion.identity);
            human.GetComponent<ColorGene>().Red = UnityEngine.Random.Range(0.0f, 1.0f);
            human.GetComponent<ColorGene>().Green = UnityEngine.Random.Range(0.0f, 1.0f);
            human.GetComponent<ColorGene>().Blue = UnityEngine.Random.Range(0.0f, 1.0f);
            human.transform.localScale = new Vector2(UnityEngine.Random.Range(0.2f, 0.26f), UnityEngine.Random.Range(0.1f, 0.26f));
            population.Add(human);
        }
    }

    private void SpawnNewPopulation()
    {
        // Se la ordinasi in ordine discendente, prenderei i piu' piccoli
        List<GameObject> newPopulation = population.OrderBy(e => e.GetComponent<DNA>().TimeToLive).ToList<GameObject>();

        population.Clear();

        // Prendo gli elementi della lista ordinata che stanno dopo la meta', ovvero quelli morti per ultimi, e quindi piu' forti
        for (int i = (int) (newPopulation.Count / 2.0f) - 1; i < newPopulation.Count - 1; i++)
        {
            // Per mantenere il numero iniziale, aggiungo due figli
            population.Add(Breed(newPopulation[i], newPopulation[i+1]));
            population.Add(Breed(newPopulation[i+1], newPopulation[i]));
        }

        newPopulation.ForEach(e => Destroy(e));

        ++generation;
        elapsed = 0.0f;
    }

    private GameObject Breed(GameObject father, GameObject mother)
    {
        Vector2 position = new Vector2(UnityEngine.Random.Range(-9.0f, 9.0f), UnityEngine.Random.Range(-4.0f, 4.0f));
        GameObject child = Instantiate(person, position, Quaternion.identity);
        ColorGene fatherGene = father.GetComponent<ColorGene>();
        ColorGene motherGene = mother.GetComponent<ColorGene>();
        if (UnityEngine.Random.Range(0, 101) < mutationRatio)
        {
            // Swappo il gene in base al valore random, tutti gli algo genetici fanno cosi'
            child.GetComponent<ColorGene>().Red = UnityEngine.Random.Range(0.0f, 1.0f) <= 0.5f ? fatherGene.Red : motherGene.Red;
            child.GetComponent<ColorGene>().Green = UnityEngine.Random.Range(0.0f, 1.0f) <= 0.5f ? fatherGene.Green : motherGene.Green;
            child.GetComponent<ColorGene>().Blue = UnityEngine.Random.Range(0.0f, 1.0f) <= 0.5f ? fatherGene.Blue : motherGene.Blue;
            child.transform.localScale = UnityEngine.Random.Range(0.0f, 1.0f) <= 0.5f ? fatherGene.transform.localScale : motherGene.transform.localScale;
        }
        else
        {
            // Introduco una possibile mutazione, che avverra' nel 15% dei casi, di solito si usa un valore del 0.5/1 %
            child.GetComponent<ColorGene>().Red = UnityEngine.Random.Range(0.0f, 1.0f);
            child.GetComponent<ColorGene>().Green = UnityEngine.Random.Range(0.0f, 1.0f);
            child.GetComponent<ColorGene>().Blue = UnityEngine.Random.Range(0.0f, 1.0f);
            child.transform.localScale = new Vector2(UnityEngine.Random.Range(0.2f, 0.26f), UnityEngine.Random.Range(0.1f, 0.26f));
        }
        return child;
    }
}
