﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ColorGene : MonoBehaviour
{
    [Range(0.0f, 1.0f)][SerializeField] private float red;
    [Range(0.0f, 1.0f)][SerializeField] private float green;
    [Range(0.0f, 1.0f)][SerializeField] private float blue;

    public float Red { get => red; set => red = value; }
    public float Green { get => green; set => green = value; }
    public float Blue { get => blue; set => blue = value; }

    public Color GetGeneColor()
    {
        return new Color(this.red, this.green, this.blue);
    }
}
