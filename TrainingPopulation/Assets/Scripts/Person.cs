﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Person : MonoBehaviour
{
    private DNA dna;

    private SpriteRenderer spriteRenderer;
    private Collider2D personCollider;

    private void Start()
    {
        dna = GetComponent<DNA>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        personCollider = GetComponent<Collider2D>();
        spriteRenderer.color = GetComponent<ColorGene>().GetGeneColor();
    }

    private void OnMouseDown()
    {
        dna.Dead = true;
        dna.TimeToLive = PopulationManager.elapsed;
        KillPerson();
    }

    private void KillPerson()
    {
        spriteRenderer.enabled = false;
        personCollider.enabled = false;
    }
}
