﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DNA : MonoBehaviour
{
    private ColorGene gene;

    private bool dead;
    private float timeToLive;

    public bool Dead { get => dead; set => dead = value; }
    public float TimeToLive { get => timeToLive; set => timeToLive = value; }

    private void Start()
    {
        gene = GetComponent<ColorGene>();
        dead = false;
        timeToLive = 0.0f;
    }
}
